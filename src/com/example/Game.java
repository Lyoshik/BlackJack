package com.example;

public class Game {

    CardNumber[] cardNumbers = new CardNumber[CardNumber.values().length];
    Suit[] suits = new Suit[Suit.values().length];
    Card[] deck = new Card[52];

    public void setCardNumbers(){
        int element = 0;
        for (CardNumber cardNumber: CardNumber.values()) {
            cardNumbers[element++] = cardNumber;
        }
    }

    public void setSuits(){
        int element = 0;
        for (Suit suit: Suit.values()) {
            suits[element++] = suit;
        }
    }

    public void setDeck(Card card){
        int element = 0;
        for (int i = 0; i < card.cardNumber.values().length; i++) {
            for (int j = 0; j < card.suit.values().length; j++) {
                deck[element] = new Card(card.cardNumber.values()[i], card.suit.values()[j]);
            }
        }
    }

    public int setValue(CardNumber cardNumber) {
        switch (cardNumber) {
            case Two:
                return 2;
            case Three:
                return 3;
            case Four:
                return 4;
            case Five:
                return 5;
            case Six:
                return 6;
            case Seven:
                return 7;
            case Eight:
                return 8;
            case Nine:
                return 9;
            case Ten:
                return 10;
            case Valet:
                return 11;
            case Quenn:
                return 12;
            case King:
                return 13;
            case Ace:
                return 1;
            default:
                return 0;
        }
    }
}
