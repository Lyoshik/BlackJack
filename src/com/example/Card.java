package com.example;

public class Card {
    Suit suit;
    CardNumber cardNumber;

    public Card(CardNumber cardNumber, Suit suit){
        this.suit = suit;
        this.cardNumber = cardNumber;
    }
}