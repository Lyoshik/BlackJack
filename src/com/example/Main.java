package com.example;

import java.util.Random;

public class Main {

    public static void main(String[] args){
        Random random = new Random();
        Game game = new Game();
        int[] totalValues = new int[52];
        CardNumber[] boardCardNumbers = new CardNumber[CardNumber.values().length];
        Suit[] boardSuits = new Suit[Suit.values().length];
        game.setCardNumbers();
        game.setSuits();

        System.out.println("Hello! Lets play BlackJack!");
        int sum = 0;

        for (int i = 0; i < 52; i++) {
            CardNumber a = game.cardNumbers[random.nextInt(game.cardNumbers.length - 1)];
            Suit b = game.suits[random.nextInt(game.suits.length - 1)];

            boardCardNumbers[i]=a;
            boardSuits[i]=b;

            System.out.println("You have " + boardCardNumbers[i] + " of " + boardSuits[i] + " and your total value is " + sum+ ".");
            totalValues[i] = game.setValue(a);

            for (int k = 0; k < totalValues.length; k++) {
                sum += totalValues[k];
            }

            if (sum >= 21) {
                System.out.println("Game is over!");
                break;
            }
        }
    }
}
